package com.weatherclear

import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.Identity
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.weatherclear.data.Location
import com.weatherclear.data.LocationGson
import com.weatherclear.data.LocationList
import com.weatherclear.databinding.ActivityMainBinding


@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    private lateinit var oneTapClient: SignInClient
    private lateinit var signInRequest: BeginSignInRequest
    public val REQ_ONE_TAP = 2
    private lateinit var binding: ActivityMainBinding
    private lateinit var navView : BottomNavigationView
    lateinit var locationList: LocationList
    var locationSelected : Location? = null
    public var nom = ""

    val activityresult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if (it.resultCode == DetailActivity.RESULT_SAVE){
            navView.selectedItemId = R.id.navigation_home
            val intent = it.data
            if (intent != null){
                val locationJson = intent.getStringExtra("location")
                if (locationJson != null){
                    val location = LocationGson().locationFromJson(locationJson)
                    locationList.add(location)
                }
            }
        } else if (it.resultCode == DetailActivity.RESULT_UPDATE){
            val intent = it.data
            if (intent != null && locationSelected != null){
                val fav = intent.getBooleanExtra("fav", false)
                locationSelected!!.updateFav(fav)
                locationSelected = null
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        oneTapClient = Identity.getSignInClient(this)
        signInRequest = BeginSignInRequest.builder()
            .setPasswordRequestOptions(BeginSignInRequest.PasswordRequestOptions.builder()
                .setSupported(true)
                .build())
            .setGoogleIdTokenRequestOptions(
                BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                    .setSupported(true)
                    // Your server's client ID, not your Android client ID.
                    .setServerClientId(getString(R.string.serveurID))
                    // Only show accounts previously used to sign in.
                    .setFilterByAuthorizedAccounts(false)
                    .build())
            // Automatically sign in when exactly one credential is retrieved.
            .setAutoSelectEnabled(true)
            .build()

        oneTapClient.beginSignIn(signInRequest)
            .addOnSuccessListener(this) { result ->
                try {

                    Log.i("truc","machin")
                    startIntentSenderForResult(
                        result.pendingIntent.intentSender, 2,
                        null, 0, 0, 0, null)
                } catch (e: IntentSender.SendIntentException) {
                    Log.e("TAG", "Couldn't start One Tap UI: ${e.localizedMessage}")
                }
            }
            .addOnFailureListener(this) { e ->
                // No saved credentials found. Launch the One Tap sign-up flow, or
                // do nothing and continue presenting the signed-out UI.
                Log.d("TAG", e.localizedMessage)
            }





        locationList = LocationList(this)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        navView.setupWithNavController(navController)
    }

    override fun onStart() {
        super.onStart()

        locationList.fetchItem()
    }

    override fun onStop() {
        super.onStop()

        locationList.save()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.i("truc","PUTE")
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            2 -> {
                try {
                    val credential = oneTapClient.getSignInCredentialFromIntent(data)
                    val idToken = credential.googleIdToken
                    val username = credential.id
                    val password = credential.password
                    nom = credential.id
                    val prenom = credential.givenName
                    val image = credential.profilePictureUri

                    findViewById<TextView>(R.id.prenomGoogle).text="Connecté en tant que " + nom


                    when {
                        idToken != null -> {
                            // Got an ID token from Google. Use it to authenticate
                            // with your backend.
                            Log.i("TAG", "Got ID token.")
                        }
                        password != null -> {
                            // Got a saved username and password. Use them to authenticate
                            // with your backend.
                            Log.i("TAG", "Got password.")
                        }
                        else -> {
                            // Shouldn't happen.
                            Log.i("TAG", "No ID token or password!")
                        }
                    }
                } catch (e: ApiException) {
                    when (e.statusCode) {
                        CommonStatusCodes.CANCELED -> {
                            Log.i("truc", "One-tap dialog was closed.")

                        }
                        CommonStatusCodes.NETWORK_ERROR -> {
                            Log.i("truc", "One-tap encountered a network error.")
                            // Try again or just ignore.
                        }
                        else -> {
                            Log.i("truc", "Couldn't get credential from result." +
                                    " (${e.localizedMessage})")
                        }
                    }
                }

            }
        }
    }


}