package com.weatherclear

import android.app.Application
import android.content.Context

class WeatherApp : Application() {
    companion object{
        lateinit var app : WeatherApp

        fun getContext() : Context{
            return app.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        app = this
    }


}