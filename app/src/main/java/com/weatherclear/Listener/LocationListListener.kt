package com.weatherclear.Listener

interface LocationListListener {
    fun notifyUpdate()

    fun notifyItemChanged(index : Int)
}