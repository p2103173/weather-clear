package com.weatherclear.Listener

import com.weatherclear.data.Location

interface LocationListener {
    fun notifyUpdate(location : Location)

    fun notifyDeleted(location: Location)
}