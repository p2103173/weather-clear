package com.weatherclear.api

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


class OpenWeatherApi{

    object RetroFit {
        private val BASE_URL : String = "https://api.openweathermap.org/"
        private val gson : Gson by lazy {
            GsonBuilder().setLenient().create()
        }
        private val httpClient : OkHttpClient by lazy {
            OkHttpClient.Builder().build()
        }
        private val retrofit : Retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        val endpoints :  RetroFitEndpoint by lazy{
            retrofit.create(RetroFitEndpoint::class.java)
        }
    }

    interface RetroFitEndpoint{
        @GET("/data/3.0/onecall?appid=43c34bfa12148e904fcefd62414eef25&exclude=minutely,daily&units=metric")
        fun getWeather(@Query("lat") lat: Double, @Query("lon") long : Double) : Call<OpenWeatherResponseModel>
    }

    fun getWeatherForCoords(lat : Double, long : Double, callback : (res : OpenWeatherResponseModel) -> Unit){
        try {
            RetroFit.endpoints.getWeather(lat, long).enqueue(object : Callback<OpenWeatherResponseModel> {
                override fun onFailure(call: Call<OpenWeatherResponseModel>?, t: Throwable?) {
                    Log.e("retrofit", "call failed")
                    if (t != null) {
                        t.message?.let { Log.e("retrofit", it) }
                    }
                }

                override fun onResponse(call: Call<OpenWeatherResponseModel>?, response: Response<OpenWeatherResponseModel>?) {
                    if (call != null) {
                        Log.w("URLCalled", call.request().url().toString())
                    }
                    if (response != null && response.isSuccessful){
                        callback(response.body())
                    } else {
                        Log.e("retrofit", "error")
                    }
                }
            })
        } catch (e : java.lang.Exception) {
            Log.e("API", e.toString())
        }
    }
}