package com.weatherclear.api

import com.google.gson.annotations.SerializedName

data class OpenWeatherResponseModel(
    @SerializedName("lat")
    val lattitude : Double,
    @SerializedName("lon")
    val longitude : Double,
    @SerializedName("timezone")
    val timezone : String,
    @SerializedName("current")
    val current : Forecast,
    @SerializedName("hourly")
    val hourly : List<Forecast>
){
    data class Forecast(
        @SerializedName("dt")
        val timestamp : String,
        @SerializedName("temp")
        val temp : Double,
        @SerializedName("visibility")
        val visibility : Double,
        @SerializedName("feels_like")
        val temp_feel : Double,
        @SerializedName("wind_speed")
        val wind_speed : Double,
        @SerializedName("wind_deg")
        val wind_dir : String,
        @SerializedName("humidity")
        val humidity : Double,
        @SerializedName("weather")
        val weather: List<Weather?>

    )

    data class Weather(
        @SerializedName("id")
        val id : Int,
        @SerializedName("main")
        val type : String,
        @SerializedName("icon")
        val icon : String
    )
}