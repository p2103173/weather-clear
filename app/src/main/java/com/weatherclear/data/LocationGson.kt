package com.weatherclear.data

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import com.weatherclear.api.OpenWeatherResponseModel
import java.lang.reflect.Type
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class LocationGson : JsonDeserializer<Location>, JsonSerializer<Location> {
    private val gson : Gson = GsonBuilder().registerTypeAdapter(Location::class.java, this).create()

    fun locationToJson(location: Location) : String{
        return gson.toJson(location)
    }

    fun locationFromJson(src : String) : Location{
        return gson.fromJson(src, Location::class.java)
    }

    fun listToJson(list: LocationList) : String{
        return gson.toJson(list)
    }

    fun listFromJson(src : String) : List<Location>?{
        return gson.fromJson<List<Location>?>(src, object : TypeToken<List<Location>?>() {}.type)
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Location? {
        val jobject = json?.asJsonObject
        if (jobject != null){
            val location = Location(jobject.get("latitude").asFloat, jobject.get("longitude").asFloat)
            location.name = jobject.get("name").asString
            location.country = jobject.get("country").asString

            location.fav = jobject.get("fav").asBoolean

            location.data = gson.fromJson<OpenWeatherResponseModel?>(jobject.get("data").asString, OpenWeatherResponseModel::class.java)

            location.lastUpdate = LocalDateTime.parse(jobject.get("lastupdate").asString, DateTimeFormatter.ISO_DATE_TIME)

            return location
        }
        return null
    }

    override fun serialize(src: Location?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        val obj = JsonObject();
        obj.addProperty("latitude", src?.latitude)
        obj.addProperty("longitude", src?.longitude)

        obj.addProperty("lastupdate", src?.lastUpdate?.format(DateTimeFormatter.ISO_DATE_TIME))

        obj.addProperty("name", src?.name)
        obj.addProperty("country", src?.country)

        obj.addProperty("data", gson.toJson(src?.data))

        obj.addProperty("fav", src?.fav)

        return obj
    }
}