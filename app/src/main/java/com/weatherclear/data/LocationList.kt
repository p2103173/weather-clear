package com.weatherclear.data

import android.content.Context
import android.util.Log
import com.weatherclear.Listener.LocationListListener
import com.weatherclear.Listener.LocationListener

class LocationList(private val context : Context) : ArrayList<Location>() {
    class LocationListenerForLocationList(val parent: LocationList) : LocationListener {
        override fun notifyUpdate(location: Location) {
            parent.update(location)
        }

        override fun notifyDeleted(location: Location) {
            location.removeListener(this)
            parent.remove(location)
        }

    }

    private val filename = "data"
    private val locationListener = LocationListenerForLocationList(this)
    private val listeners = ArrayList<LocationListListener>()

    init {
        this.load()
        this.fetchItem()
    }

    fun addListener(listener: LocationListListener){
        if (!listeners.contains(listener)){
            listeners.add(listener)
        }
    }

    fun removeListener(listener: LocationListListener){
        if (listeners.contains(listener)){
            listeners.remove(listener)
        }
    }


    fun save(){
        try {
            context.openFileOutput(filename, Context.MODE_PRIVATE).use {
                it.write(LocationGson().listToJson(this).toByteArray())
            }
        } catch (e : Exception){
            Log.e("GSONSave", e.toString())
        }
    }

    fun load(){
        try {
            var file : String = "";
            //Log.w("Context", context.toString())
            context.openFileInput(filename).bufferedReader().useLines { lines ->
                lines.forEach { elt -> file+=elt}
            }
            Log.w("FileOpened", file)
            val imported = LocationGson().listFromJson(file)
            imported?.forEach { elt ->
                this.add(elt)
            }
        } catch (e : Exception){
            Log.e("GSONLoad", e.toString())
        }
    }

    fun update(item : Location){
        Log.w("List", "one item updated")
        notifyListenerOneUpdate(this.indexOf(item))
    }

    fun fetchItem(){
        this.forEach {
            it.fetch()
        }
    }

    private fun notifyListenerOneUpdate(index : Int){
        listeners.forEach {
            it.notifyItemChanged(index)
        }
    }

    private fun notifyListenerUpdate(){
        listeners.forEach {
            it.notifyUpdate()
        }
    }

    override fun add(item : Location) : Boolean{
        if (!this.contains(item)){
            Log.w("List", "item added")
            item.addListener(locationListener)
            item.fetch()
            if (super.add(item)){
                notifyListenerUpdate()
                this.save()
                return true
            }
        }
        return false
    }

    override fun remove(item: Location): Boolean {
        Log.w("List", "item removed")
        if (super.remove(item)){
            item.removeListener(locationListener)
            notifyListenerUpdate()
            this.save()
            return true
        }
        return false
    }

}