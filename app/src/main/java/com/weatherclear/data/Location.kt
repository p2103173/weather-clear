package com.weatherclear.data

import android.location.Geocoder
import android.util.Log
import com.weatherclear.Listener.LocationListener
import com.weatherclear.WeatherApp
import com.weatherclear.api.OpenWeatherApi
import com.weatherclear.api.OpenWeatherResponseModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.time.LocalDateTime
import java.util.*


class Location(val latitude: Float, val longitude: Float){
    var fav : Boolean = false

    var data : OpenWeatherResponseModel? = null
    var name : String? = null
    var country : String? = null
    var lastUpdate : LocalDateTime? = null

    private val listeners = ArrayList<LocationListener>()

    fun fetch(){
        Log.w("Location", "trytofetch");
        if (lastUpdate == null || lastUpdate!!.plusMinutes(3) < LocalDateTime.now()){
            lastUpdate = LocalDateTime.now()
            Log.w("Location", "fetch");
            runBlocking {
                launch {
                    val address = Geocoder(WeatherApp.getContext(), Locale.getDefault()).getFromLocation(latitude.toDouble(), longitude.toDouble(), 1).get(0)
                    name = address.locality
                    country = address.countryCode
                    OpenWeatherApi().getWeatherForCoords(latitude.toDouble(), longitude.toDouble()){ res ->
                        updateData(res)
                    }
                }
            }
        }
    }

    fun updateFav(value : Boolean){
        fav = value
        this.notifyUpdate()
    }

    fun updateData(data : OpenWeatherResponseModel?){
        this.data = data
        this.notifyUpdate()
    }

    private fun notifyUpdate(){
        listeners.forEach {
            it.notifyUpdate(this)
        }
    }

    private fun notifyDelete(){
        listeners.forEach {
            it.notifyDeleted(this)
        }
    }

    fun addListener(listener: LocationListener){
        if (!listeners.contains(listener)){
            listeners.add(listener)
        }
    }

    fun removeListener(listener: LocationListener){
        if (listeners.contains(listener)){
            listeners.remove(listener)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (!(other is Location)){
            return false
        }

        val o = other as Location
        return (o.latitude === this.latitude) && (o.longitude === this.longitude)
    }

}