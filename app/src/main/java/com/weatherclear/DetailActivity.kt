package com.weatherclear

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.weatherclear.Listener.LocationListener
import com.weatherclear.api.OpenWeatherResponseModel
import com.weatherclear.data.Location
import com.weatherclear.data.LocationGson
import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.round


class DetailActivity : AppCompatActivity() {
    companion object{
        val RESULT_SAVE = 1001      //Quand on va sauvegarder une Location
        val RESULT_UPDATE = 1002    //Quand on va modifier Favoris/Pas Favoris sur une Location

        val CONTEXT_SAVE = 2001
        val CONTEXT_UPDATE = 2002
    }

    lateinit var location : Location

    var fav : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        Log.e("Detail Activity", "OnCREATE")

        val resultFromIntent = intent.getStringExtra("location")
        if (resultFromIntent != null){
            location = LocationGson().locationFromJson(resultFromIntent)
        } else {
            closeCancel()
        }



        val context = intent.getIntExtra("context", CONTEXT_UPDATE)
        if (context == CONTEXT_SAVE){
            findViewById<ImageView>(R.id.favImage).setOnClickListener {
                closeSave()
            }
            findViewById<ImageView>(R.id.closeImage).setOnClickListener {
                closeCancel()
            }

        } else {
            updateFav(location.fav)
            Log.e("Fav", location.fav.toString())
            findViewById<ImageView>(R.id.favImage).setOnClickListener {
                updateFav(!fav)
            }

            findViewById<ImageView>(R.id.closeImage).setOnClickListener {
                closeUpdate()
            }
        }


        location.addListener(object : LocationListener{
            override fun notifyUpdate(location: Location) {
                showData()
            }

            override fun notifyDeleted(location: Location) {
                closeCancel()
            }
        })
        showData()
    }

    override fun onStart() {
        super.onStart()
        Log.e("Detail Activity", "OnStart")

        location.fetch()
    }

    fun closeCancel(){
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun closeSave(){
        val intent = Intent()
        intent.putExtra("location", LocationGson().locationToJson(location))
        setResult(RESULT_SAVE, intent)
        finish()
    }

    fun closeUpdate(){
        val intent = Intent()
        intent.putExtra("fav", fav)
        setResult(RESULT_UPDATE, intent)
        finish()
    }

    fun updateFav(value : Boolean){
        fav = value
        val imageView = findViewById<ImageView>(R.id.favImage)
        if (fav == true){
            imageView.setImageResource(R.drawable.star_solid)
        } else {
            imageView.setImageResource(R.drawable.star_regular)
        }
    }

    fun showData(){
        if (location != null && location.data != null){
            val timezone = TimeZone.getTimeZone(location.data?.timezone)

            val nomVille = findViewById<TextView>(R.id.nomVille)
            val date = findViewById<TextView>(R.id.date)

            val temperatureDetail = findViewById<TextView>(R.id.temperatureDetail)
            val imageMeteo = findViewById<ImageView>(R.id.imageMeteoDetail)

            val liste_heures = findViewById<RecyclerView>(R.id.listeHeures)
            var layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            liste_heures.layoutManager = layoutManager

            val heures_value = ArrayList<OpenWeatherResponseModel.Forecast>(location.data!!.hourly)
            heures_value.removeAt(0)

            val adapter = HeureAdapter(heures_value, timezone)
            liste_heures.adapter = adapter


            val temperatures_value = findViewById<TextView>(R.id.temperatures_value)
            temperatures_value.text = location.data!!.current.temp_feel.let { Math.round(it).toString() } +"°C"

            val humidite_value = findViewById<TextView>(R.id.humidite_value)
            humidite_value.text = location.data!!.current.humidity.toString() + " %"

            val wind_value = findViewById<TextView>(R.id.wind_value)
            wind_value.text = location.data!!.current.wind_dir + "° " + location.data!!.current.wind_speed.toString() + "m/s"

            val image_uv = findViewById<TextView>(R.id.uv_value)
            image_uv.text = (round(location.data!!.current.visibility/100)/10).toString() + " km"

            nomVille.setText(location.name)

            val time = LocalDateTime.ofInstant(Instant.ofEpochSecond(location.data?.current?.timestamp!!.toLong()), timezone.toZoneId())
            date.setText(time.format(DateTimeFormatter.ofPattern("HH:mm")))


            imageMeteo.setImageResource(WeatherApp.app.resources.getIdentifier("icon"+location.data!!.current.weather.get(0)?.icon, "drawable", WeatherApp.app.packageName))
            temperatureDetail.setText(location.data!!.current.temp.let { Math.round(it).toString() } +"°C")

        }
    }
}