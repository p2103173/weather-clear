package com.weatherclear

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.weatherclear.api.OpenWeatherResponseModel
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*
import kotlin.collections.ArrayList

class HeureAdapter(private val dataSet: ArrayList<OpenWeatherResponseModel.Forecast>, val timeZone: TimeZone) :
    RecyclerView.Adapter<HeureAdapter.HeureListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeureAdapter.HeureListViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.hour_item, parent, false)
        return HeureListViewHolder(view)
    }

    override fun onBindViewHolder(holder: HeureAdapter.HeureListViewHolder, position: Int) {
        val forecast = dataSet.get(position)
        holder.bind(forecast)
    }

    inner class HeureListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        var jour: TextView
        var heure: TextView
        var temp: TextView
        var image: ImageView

        init {
            jour = itemView.findViewById(R.id.jour)
            heure = itemView.findViewById(R.id.heure)
            temp = itemView.findViewById(R.id.temp)
            image = itemView.findViewById(R.id.image)

        }


        fun bind(forecast: OpenWeatherResponseModel.Forecast){
            val heure_value = LocalDateTime.ofInstant(Instant.ofEpochSecond(forecast?.timestamp!!.toLong()), timeZone.toZoneId())
            //SimpleDateFormat("EEEE", Locale.FRENCH).format()
            jour.text = heure_value.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.FRENCH)
            heure.text = heure_value.format(DateTimeFormatter.ofPattern("HH:mm"))
            temp.text = forecast?.temp.let { Math.round(it!!).toString() } +"°C"
            image.setImageResource(WeatherApp.app.resources.getIdentifier("icon"+forecast.weather.get(0)?.icon, "drawable", WeatherApp.app.packageName))
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }
}