package com.weatherclear.ui.map

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.weatherclear.R
import com.weatherclear.WeatherApp
import com.weatherclear.data.Location

class LocationInfoWindow(val context: Context) : GoogleMap.InfoWindowAdapter{
    lateinit var inflater : LayoutInflater

    override fun getInfoContents(p0: Marker): View? {
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.location_info_window_map, null)

        val location = (p0.tag as Location?)
        if (location != null && location.data != null){
            view.findViewById<TextView>(R.id.textView7).text = location.name
            view.findViewById<TextView>(R.id.textView8).text = location.data!!.current.temp.let { Math.round(it).toString() } +"°C"
            view.findViewById<ImageView>(R.id.imageView).setImageResource(WeatherApp.app.resources.getIdentifier("icon"+location.data!!.current.weather.get(0)?.icon, "drawable", WeatherApp.app.packageName))
        }

        return view
    }

    override fun getInfoWindow(p0: Marker): View? {
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.location_info_window_map, null)

        val location = (p0.tag as Location?)
        if (location != null && location.data != null){
            view.findViewById<TextView>(R.id.textView7).text = location.name
            view.findViewById<TextView>(R.id.textView8).text = location.data!!.current.temp.let { Math.round(it).toString() } +"°C"
            view.findViewById<ImageView>(R.id.imageView).setImageResource(WeatherApp.app.resources.getIdentifier("icon"+location.data!!.current.weather.get(0)?.icon, "drawable", WeatherApp.app.packageName))
        }

        return view
    }
}