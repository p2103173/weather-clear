package com.weatherclear.ui.map

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.weatherclear.DetailActivity
import com.weatherclear.Listener.LocationListener
import com.weatherclear.MainActivity
import com.weatherclear.R
import com.weatherclear.data.Location
import com.weatherclear.data.LocationGson
import com.weatherclear.databinding.FragmentMapBinding

class MapFragment : Fragment(), OnMapReadyCallback  {

    private lateinit var mMap: GoogleMap
    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)

        _binding = FragmentMapBinding.inflate(inflater, container, false)
        val root: View = binding.root

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0

        val list = (activity as MainActivity).locationList
        var marker : Marker? = null
        var listener = object : LocationListener{
            override fun notifyUpdate(location: Location) {
                if (marker != null && location.data != null) {
                    marker?.tag = location
                    marker?.showInfoWindow()
                }
            }

            override fun notifyDeleted(location: Location) {
                marker?.remove()
            }

        }

        val locationInfoWindow = LocationInfoWindow(requireContext())
        mMap.setInfoWindowAdapter(locationInfoWindow)

        val sydney = LatLng(45.76388414568688, 4.837043421333269)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        mMap.moveCamera(CameraUpdateFactory.zoomTo(10F))


        mMap.setOnMapLongClickListener {
            Log.e("Coords", it.toString())

            //Suppression ancien tag
            (marker?.tag as Location?)?.removeListener(listener)
            marker?.remove()

            //Recentrage caméra
            val campos = CameraUpdateFactory.newCameraPosition(CameraPosition.Builder().target(it).zoom(12F).build())
            mMap.animateCamera(campos, 1000, null)

            //Création marker
            marker = mMap.addMarker(MarkerOptions().position(it))

            //Création Location
            val location = Location(it.latitude.toFloat() ,it.longitude.toFloat())
            location.addListener(listener)
            location.fetch()
        }


        mMap.setOnInfoWindowClickListener {
            Log.e("Ok", "Click")
            val location = (it.tag as Location)
            if (location != null){
                //list.add(location)
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("location", LocationGson().locationToJson(location!!))
                intent.putExtra("context", DetailActivity.CONTEXT_SAVE)
                (activity as MainActivity).activityresult.launch(intent)
            }
            /*val intent = Intent(fragment.context, DetailActivity::class.java)
            intent.putExtra("location", LocationGson().locationToJson(currentLoc!!))
            (fragment.activity as MainActivity).activityresult.launch(intent)*/
            //(activity as MainActivity)

        }

        mMap.setOnMapClickListener {
            Log.e("MAP", "CLICK")
            true
        }

        mMap.setOnMarkerClickListener {
            Log.e("MAP", "FOCUS")
            true
        }


    }
}