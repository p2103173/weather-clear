
package com.weatherclear.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.weatherclear.DetailActivity
import com.weatherclear.Listener.LocationListListener
import com.weatherclear.MainActivity
import com.weatherclear.R
import com.weatherclear.WeatherApp
import com.weatherclear.data.Location
import com.weatherclear.data.LocationGson
import com.weatherclear.data.LocationList


class LocationListAdapter(val list: LocationList, val fragment: HomeFragment) : ListAdapter<Location, LocationListAdapter.LocationListViewHolder>( LocationComparator ){
    class LocationListListenerForLocationListAdapter(val parent: LocationListAdapter) : LocationListListener {
        override fun notifyUpdate() {
            parent.notifyDataSetChanged()
        }

        override fun notifyItemChanged(index: Int) {
            parent.notifyDataSetChanged()
            //parent.notifyItemChanged(index)
        }
    }

    val locationListListener = LocationListListenerForLocationListAdapter(this)

    private var showOnlyFav = false

    init {
        list.addListener(locationListListener)
        list.fetchItem()
        this.submitList(list)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.lineweather, parent, false)
        return LocationListViewHolder(view)
    }
    override fun onBindViewHolder(holder: LocationListViewHolder, position: Int) {
        val location = getItem(position)
        holder.bind(location)
    }

    fun setShowOnlyFav(value : Boolean){
        showOnlyFav = value
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        if (showOnlyFav){
            return this.currentList.filter { l -> l.fav }.size
        }
        return this.currentList.size
    }

    override fun getItem(position: Int): Location {
        if (showOnlyFav){
            return this.currentList.filter { l -> l.fav }.get(position)
        }
        return this.currentList.get(position)
    }

    fun destroy(){
        list.removeListener(locationListListener)
    }

    inner class LocationListViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        var currentLoc : Location? = null
        val favImage : ImageView = itemView.findViewById(R.id.imageFavLine)
        var nomLocation: TextView = itemView.findViewById(R.id.nomLocation)
        var temperatureLocation: TextView = itemView.findViewById(R.id.temperatureLocation)
        var codeLocation : TextView = itemView.findViewById(R.id.coords)
        var imageLocation:ImageView = itemView.findViewById(R.id.imageMeteo)

        init {
            itemView.setOnLongClickListener {
                Log.e("Click", "Click")
                if (currentLoc != null) {
                    list.remove(currentLoc)
                }
                true
            }

            itemView.setOnClickListener{
                if (currentLoc != null) {
                    val activity = (fragment.activity as MainActivity)

                    val intent = Intent(fragment.context, DetailActivity::class.java)
                    intent.putExtra("location", LocationGson().locationToJson(currentLoc!!))
                    intent.putExtra("context", DetailActivity.CONTEXT_UPDATE)
                    activity.activityresult.launch(intent)
                    activity.locationSelected = currentLoc
                }
            }
        }
        fun bind(location: Location)
        {
            currentLoc = location

            if (location.data != null){
                if (location.fav){
                    favImage.visibility = View.VISIBLE
                } else {
                    favImage.visibility = View.GONE
                }
                codeLocation.text = location.country
                nomLocation.text = location.name
                temperatureLocation.text = location.data!!.current.temp.let { Math.round(it).toString() } +"°C"
                imageLocation.setImageResource(WeatherApp.app.resources.getIdentifier("icon"+location.data!!.current.weather.get(0)?.icon, "drawable", WeatherApp.app.packageName))

            } else {
                nomLocation.text="Chargement"
            }


        }
    }
    object LocationComparator : DiffUtil.ItemCallback<Location>() {
        override fun areItemsTheSame(oldItem: Location, newItem: Location): Boolean {
            return oldItem.equals(newItem)
        }

        override fun areContentsTheSame(oldItem: Location, newItem: Location): Boolean {
            return oldItem.equals(newItem)
        }
    }
}