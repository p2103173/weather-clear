package com.weatherclear.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.weatherclear.MainActivity
import com.weatherclear.R
import com.weatherclear.data.Location
import com.weatherclear.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private lateinit var locationListAdapter : LocationListAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        val homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val locationList = (activity as MainActivity).locationList

        locationListAdapter = LocationListAdapter(locationList, this)
        binding.ISSOU.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(view.context)
            adapter = locationListAdapter
        }


        var nomHome = (activity as MainActivity).nom
        if(nomHome!="") {
            binding.prenomGoogle.text="Connecté en tant que " + (activity as MainActivity).nom
        }


        binding.switchFav.findViewById<Switch>(R.id.switchFav).setOnCheckedChangeListener { compoundButton, b ->
            Log.e("Change", b.toString())
            locationListAdapter.setShowOnlyFav(b)
        }



        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        if (locationListAdapter != null){
            locationListAdapter.destroy()
        }
    }

}